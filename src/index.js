import React from "react";
import ReactDOM from "react-dom/client";
import Main from "./components/main";
import './style.css'
import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";

  const router = createBrowserRouter([
    {
      path: "/currency",
      element: <div>Hello world!</div>,
    },
  ]);


const root = ReactDOM.createRoot(document.querySelector("main"));
root.render(<Main />);
