import React from 'react'

import './homepage.css'

export default function Homepage(){
    return(
        <div><p className='home-title'>Вітаємо! Для відстежування інформації скористайтеся кнопками навігації</p></div>
    )
}