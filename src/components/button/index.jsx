import React from 'react'

import './button.css'

export default function Button ({value}){
    return(
        <div>
        <input className='button' type="button" value={value} />
        </div>
    )
}