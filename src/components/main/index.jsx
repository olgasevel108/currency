import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Button from '../button';
import Section from '../section';
import Homepage from '../homepage'
import Date from '../date'

import './main.css';

const Main = () => {
  return (
    <Router>
      <header>
        <Link to="/currency">
          <Button value="Офіційний курс гривні до іноземних валют та облікова ціна банківських металів" />
        </Link>
        <Link to="/bank-info">
          <Button value="Основні показники діяльності банків України" />
        </Link>
        <Link to="/">
          <Button value="Домашня сторінка" />
        </Link>
      </header>
      <Routes>
        <Route path="/currency" element={<Section url="https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json" />} />
        <Route path="/bank-info" element={<Section url="https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json" />} />
      <Route path='/' element={<Homepage />} />
      </Routes>
    </Router>
  );
};

export default Main;
