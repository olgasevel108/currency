import React, { useState, useEffect } from 'react';
import { data } from '../reques';
import LinearProgress from '@mui/material/LinearProgress';
import './section.css'
import Date from '../date'

const Section = ({ url }) => {
  const [allCurrency, setAllCurrency] = useState(null);

  useEffect(() => {
    data(url).then((data1) => {
      setAllCurrency(data1);
    });
  }, [url]);

  return (
    <div>
      <Date></Date>
      <table className='table'>
        <tbody>
          <tr>
            <th>Currency Name</th>
            <th>Price</th>
            <th>Code</th>
          </tr>
          {Array.isArray(allCurrency) ? (
            allCurrency.map((item, index) => {
              return (
                <tr key={index * 5 + 't'}>
                  <td>{item.txt}</td>
                  <td>{item.rate ? item.rate.toFixed(2) : item.value}</td>
                  <td>{item.cc}</td>
                </tr>
              );
            })
          ) : (
            <LinearProgress />
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Section;
