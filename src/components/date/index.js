import React, { useEffect, useState } from 'react';
import { data } from '../reques';

import './date.css'

const Date = () =>{
    const [exchangeDate, setExchangeDate] = useState('none')
    useEffect(()=>{
        const fetchData = async()=>{
            try{
                const result = await data('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
                if (Array.isArray(result) && result.length>0){
                    setExchangeDate(result[0].exchangedate)
                }
                }catch(error){
                    console.error(error)
            }
        }
        fetchData();
    }, [])
    return(
        <div>
            <p className='exchangedate'>exchangeDate: {exchangeDate}</p>
        </div>
    )

}

export default Date;